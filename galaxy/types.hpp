/* ************************************************************************** */
/*                                                                            */
/*                                                    :::::      ::::::::::   */
/*   types.h                                         :+ :+:       :+:         */
/*                                                  +:  +:+        +:+        */
/*   By: amaury <amaury@deltasigma.fr>             +#   +#+        +#+#+      */
/*                                                +#    +#+       +#+         */
/*   Created: 2024/04/22 21:43:00 by amaury      #+     #+#    #+#            */
/*   Updated: 2024/04/22 21:43:00 by amaury     ###########   #########.fr    */
/*                                                                            */
/* ************************************************************************** */

#ifndef TYPES_H
# define TYPES_H

# include <cstdint>
# include <cstddef>
# include <cstdlib>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef size_t usize;

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;
typedef ssize_t isize;

typedef float f32;
typedef double f64;

#endif // TYPES_H