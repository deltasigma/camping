#ifndef LOG_HPP
# define LOG_HPP

# include <spdlog/spdlog.h>

# define LOG_TRACE(fmt, ...) spdlog::trace(fmt, ##__VA_ARGS__)
# define LOG_DEBUG(fmt, ...) spdlog::debug(fmt, ##__VA_ARGS__)
# define LOG_INFO(fmt, ...) spdlog::info(fmt, ##__VA_ARGS__)
# define LOG_WARN(fmt, ...) spdlog::warn(fmt, ##__VA_ARGS__)
# define LOG_ERROR(fmt, ...) spdlog::error(fmt, ##__VA_ARGS__)
# define LOG_CRITICAL(fmt, ...) spdlog::critical(fmt, ##__VA_ARGS__)

#endif // LOG_HPP
