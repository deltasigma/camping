#ifndef VEC_HPP
# define VEC_HPP

# include <array>
# include "types.hpp"

// General n-dimensional vector

template<typename T, usize N>
class vector_base {
public:
	template<class ...Args>
	explicit vector_base(Args &&... args) : data{std::forward<Args>(args)...} {}
	vector_base() : data{0} {}
	explicit vector_base(const T value) {
		for (usize i = 0; i < N; i++)
			data[i] = value;
	}

	vector_base<T, N> &operator=(const vector_base<T, N> &v) {
		if (this != &v) {
			for (usize i = 0; i < N; i++)
				data[i] = v[i];
		}
		return *this;
	}

	T &operator[](usize i) { return data[i]; }
	T operator[](usize i) const { return data[i]; }

protected:
	std::array<T, N> data;
};

template<typename T, usize N>
class vector : public vector_base<T, N> {
public:
	using vector_base<T, N>::vector_base;

	T length_squared() const {
		T result = 0;
		for (usize i = 0; i < N; i++)
			result += this->data[i] * this->data[i];
		return result;
	}

	T length() const {
		T result = 0;
		for (usize i = 0; i < N; i++)
			result += this->data[i] * this->data[i];
		return sqrt(result);
	}

	inline vector<T, N> min(const vector<T, N> &v) const {
		vector<T, N> result;
		for (usize i = 0; i < N; i++)
			result[i] = std::min(this->data[i], v[i]);
		return result;
	}

	inline vector<T, N> max(const vector<T, N> &v) const {
		vector<T, N> result;
		for (usize i = 0; i < N; i++)
			result[i] = std::max(this->data[i], v[i]);
		return result;
	}

	inline T max_element() const {
		T result = this->data[0];
		for (usize i = 1; i < N; i++)
			result = std::max(result, this->data[i]);
		return result;
	}

	inline T min_element() const {
		T result = this->data[0];
		for (usize i = 1; i < N; i++)
			result = std::min(result, this->data[i]);
		return result;
	}

	inline vector<T, N> &operator=(const vector<T, N> &v) {
		if (this != &v) {
			for (usize i = 0; i < N; i++)
				this->data[i] = v[i];
		}
		return *this;
	}

	inline vector<T, N> &operator+=(const vector<T, N> &v) {
		for (usize i = 0; i < N; i++)
			this->data[i] += v[i];
		return *this;
	}

	inline vector<T, N> &operator-=(const vector<T, N> &v) {
		for (usize i = 0; i < N; i++)
			this->data[i] -= v[i];
		return *this;
	}

	inline vector<T, N> &operator*=(const T &v) {
		for (usize i = 0; i < N; i++)
			this->data[i] *= v;
		return *this;
	}

	inline friend vector<T, N> operator+(const vector<T, N> &lhs,
										 const vector<T, N> &rhs) {
		vector<T, N> result;
		for (usize i = 0; i < N; i++)
			result[i] = lhs[i] + rhs[i];
		return result;
	}

	inline friend vector<T, N> operator-(const vector<T, N> &lhs,
										 const vector<T, N> &rhs) {
		vector<T, N> result;
		for (usize i = 0; i < N; i++)
			result[i] = lhs[i] - rhs[i];
		return result;
	}

	inline friend vector<T, N> operator/(const vector<T, N> &lhs,
										 const T &rhs) {
		vector<T, N> result;
		for (usize i = 0; i < N; i++)
			result[i] = lhs[i] * (1 / rhs);
		return result;
	}

	inline friend vector<T, N> operator*(const vector<T, N> &lhs,
										 const T &rhs) {
		vector<T, N> result;
		for (usize i = 0; i < N; i++)
			result[i] = lhs[i] * rhs;
		return result;
	}

	inline friend vector<T, N> operator*(const T &lhs,
										 const vector<T, N> &rhs) {
		vector<T, N> result;
		for (usize i = 0; i < N; i++)
			result[i] = lhs * rhs[i];
		return result;
	}
};

// Aliases for common vector sizes (if no specializations are needed)

template<typename T> using vector1 = vector<T, 1>;
template<typename T> using vector2 = vector<T, 2>;
template<typename T> using vector3 = vector<T, 3>;
template<typename T> using vector4 = vector<T, 4>;
template<typename T> using vector5 = vector<T, 5>;
template<typename T> using vector6 = vector<T, 6>;

// Aliases for common number types (if no specializations are needed)

using f32x1 = vector1<f32>;
using f32x2 = vector2<f32>;
using f32x3 = vector3<f32>;
using f32x4 = vector4<f32>;
using f32x5 = vector5<f32>;
using f32x6 = vector6<f32>;

using f64x1 = vector1<f64>;
using f64x2 = vector2<f64>;
using f64x3 = vector3<f64>;
using f64x4 = vector4<f64>;
using f64x5 = vector5<f64>;
using f64x6 = vector6<f64>;

//----------------------------------------------------------------------------//
// TEMPLATE CLASS SPECIALIZATIONS                                             //
//----------------------------------------------------------------------------//

// This is done only for the very common two and three-dimensional vectors
// that will benefit from access to the individual components as x, y, (z)
// r, g, (b).
// This will also allow us to create functions that are specific to these
// vectors, such as cross product for vec3.

// See: vec2.hpp, vec3.hpp, ...

//----------------------------------------------------------------------------//
// OVERLOADING OPERATORS                                                      //
//----------------------------------------------------------------------------//

// INLINE OPERATORS +=, -=, *=, /=
// - vec<T, N> += vec<T, N>
// - vec<T, N> -= vec<T, N>
// - vec<T, N> *= T
// - vec<T, N> /= T

// OPERATORS +, -, *, /
// - vec<T, N> = vec<T, N> + vec<T, N>
// - vec<T, N> = vec<T, N> - vec<T, N>
// - vec<T, N> = vec<T, N> * T
// - vec<T, N> = vec<T, N> / T
// - vec<T, N> = T * vec<T, N>
// - vec<T, N> = vec<T, N>

#endif // VEC_HPP
