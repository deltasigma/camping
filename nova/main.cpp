#include <chrono>
#include "util/log.hpp"
#include "engine/engine.hpp"
#include "engine/simple_cpu/engine_simple.hpp"
#include "engine/simple_cpu/engine_simple_st.hpp"
#include "engine/simple_cpu/engine_simple_mt.hpp"
#include "engine/barnes-hut_cpu/engine_barnes-hut.hpp"
#include "solver/solver_euler.hpp"
#include "solver/solver_verlet.hpp"

void print(buffer x, usize count) {
	auto *data = static_cast<vector<f64, 4> *>(x.data);
	for (usize i = 0; i < count; i++) {
		LOG_INFO("y[{}] r = ({}, {}), v = ({}, {})",
				 i,
				 data[i][0],
				 data[i][1],
				 data[i][2],
				 data[i][3]);
	}
}

int main() {
	spdlog::set_level(spdlog::level::trace);

	const f64 ly = 9.461e15;
	const f64 ua = 1.496e11;
	const f64 yr = 3600.0 * 24 * 365;
	const f64 dy = 3600.0 * 24;

	const usize count = 16384;
	const f64 t_stop = 1.0 * yr;
	const f64 t_log = 1.0 * dy;
	const f64 dt = 1.0e-1 * dy;

	srand(42);

	std::vector<f64x4> y0(count);
	std::vector<f64> m(count);


	// Sun
	m[0] = 1.989e30;
	y0[0] = f64x4{0.0, 0.0, 0.0, 0.0};

	// Planets
	for (usize i = 1; i < count; i++) {
		m[i] = 1.0e24;

		f64 a = (f64)rand() / RAND_MAX * 2 * M_PI;
		f64 r = ((f64)rand() / RAND_MAX * 4 * ua) + 1 * ua;
		f64 s = sqrt(6.674e-11 * m[0] / r); // Using Kepler's third law

		vector<f64, 2> p{r * cos(a), r * sin(a)};
		y0[i] = f64x4{
			p[0], p[1],
			-s * sin(a),
			s * cos(a)};
	}

	engine_barneshut<f64, 2> engine(count, m);
	solver_verlet<f64, 2, 2> solver(engine, dt);
//	solver2.set_log_callback([&](f64 t, buffer y) {
//		LOG_INFO("t = {}", t);
//		print(y, count);
//	});
	LOG_INFO("Computed buffer size: {} bytes", engine.buffer_size());

	solver.run(y0.data(), t_stop, t_log);

	return 0;
}