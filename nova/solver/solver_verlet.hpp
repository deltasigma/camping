#ifndef SOLVER_VERLET_HPP
# define SOLVER_VERLET_HPP

# include "types.hpp"
# include "engine/engine.hpp"
# include "solver.hpp"

template<typename T, usize O, usize D>
class solver_verlet : public solver_base<T, O, D> {
public:
	solver_verlet(engine_base<T, O, D> &engine, T dt);
	~solver_verlet();

	T step() override;

protected:
	T dt;
	buffer k1, k2, tmp;
};

template<typename T, usize O, usize D>
solver_verlet<T, O, D>::solver_verlet(engine_base<T, O, D> &engine, T dt)
	: solver_base<T, O, D>(engine), dt(dt) {
	k1 = engine.buffer_create(engine.buffer_size());
	k2 = engine.buffer_create(engine.buffer_size());
	tmp = engine.buffer_create(engine.buffer_size());
}

template<typename T, usize O, usize D>
solver_verlet<T, O, D>::~solver_verlet() {
	this->engine.buffer_destroy(k1);
	this->engine.buffer_destroy(k2);
	this->engine.buffer_destroy(tmp);
}

template<typename T, usize O, usize D>
T solver_verlet<T, O, D>::step() {
	T t = this->current_time;
	buffer y = this->current_y;
	this->engine.compute_f(t, y, k1);
	this->engine.compute_mul_add(tmp, y, k1, 0.5 * dt);
	this->engine.compute_f(t + 0.5 * dt, tmp, k2);
	this->engine.compute_mul_add_inplace(y, k2, dt);
	return dt;
}

#endif // SOLVER_VERLET_HPP