#ifndef SOLVER_EULER_HPP
# define SOLVER_EULER_HPP

# include "types.hpp"
# include "engine/engine.hpp"
# include "solver.hpp"

template<typename T, usize O, usize D>
class solver_euler : public solver_base<T, O, D> {
public:
	solver_euler(engine_base<T, O, D> &engine, T dt);
	~solver_euler();

	T step() override;

protected:
	T dt;
	buffer dy;
};

template<typename T, usize O, usize D>
solver_euler<T, O, D>::solver_euler(engine_base<T, O, D> &engine, T dt)
	: solver_base<T, O, D>(engine), dt(dt) {
	dy = engine.buffer_create(engine.buffer_size());
}

template<typename T, usize O, usize D>
solver_euler<T, O, D>::~solver_euler() {
	this->engine.buffer_destroy(dy);
}

template<typename T, usize O, usize D>
T solver_euler<T, O, D>::step() {
	this->engine.compute_f(this->current_time, this->current_y, dy);
	this->engine.compute_mul_add_inplace(this->current_y, dy, dt);
	return dt;
}

#endif // SOLVER_EULER_HPP