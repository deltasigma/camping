#ifndef SOLVER_HPP
# define SOLVER_HPP

# include "util/log.hpp"
# include "types.hpp"
# include "engine/engine.hpp"

template<typename T, usize O, usize D>
class solver_base {
public:
	explicit solver_base(engine_base<T, O, D> &engine);
	~solver_base();

	virtual T step() = 0;
	void run(vector<T, O * D> *y0, T t_stop, T t_log);

protected:
	usize current_step;
	T current_time;
	buffer current_y;
	engine_base<T, O, D> &engine;
};

template<typename T, usize O, usize D>
solver_base<T, O, D>::solver_base(engine_base<T, O, D> &engine)
	: engine(engine), current_step(0), current_time(0.0) {
	current_y = engine.buffer_create(engine.buffer_size());
}

template<typename T, usize O, usize D>
solver_base<T, O, D>::~solver_base() {
	engine.buffer_destroy(current_y);
}

template<typename T, usize O, usize D>
void solver_base<T, O, D>::run(vector<T, O * D> *y0, T t_stop, T t_log) {
	// Load initial conditions
	engine.buffer_write(current_y, y0, engine.count);

	// TEST: SAVE DATA TO CSV
	FILE *fp = fopen("data.csv", "w");
	fprintf(fp, "t,x,y,vx,vy\n");
	vector<T, O * D> y[engine.count];

	T last_log = current_time;
	while (current_time < t_stop) {
		T dt = step();
		current_time += dt;
		current_step++;

		// Logging data
		if (current_time - last_log >= t_log) {
			const char *units[] = {"", "k", "M", "G", "T", "P", "E", "Z", "Y"};
			usize i = 0;
			T t = current_time / (3600 * 24 * 365);
			while (t >= 1000) {
				t /= 1000;
				i++;
			}

			LOG_INFO("Step: {}: t = {:.2f} {}yr", current_step, t, units[i]);
			last_log = current_time;


			// TEST: SAVE DATA TO CSV
			engine.buffer_read(y, current_y, engine.count);
			i = 0;
			while (i < engine.count) {
				fprintf(fp, "%lf,%lf,%lf,%lf,%lf\n",
						current_time, y[i][0], y[i][1], y[i][2], y[i][3]);
				i++;
			}
		}
	}

	// TEST: SAVE DATA TO CSV
	fclose(fp);
}

#endif // SOLVER_HPP