#ifndef ENGINE_BARNESHUT_HPP
# define ENGINE_BARNESHUT_HPP

# include <omp.h>
# include <vector>
# include <cstring>
# include "types.hpp"
# include "math/vec.hpp"
# include "engine/engine.hpp"
# include "util/log.hpp"

// Simple linear barnes hut tree algorithm implementation for CPU N-body
// simulation.

// Types of nodes in the tree:
// - Empty node: no children, no mass, no center of mass
// - Leaf node: no children, has mass, has center of mass
// - Internal node: has children, has mass, has center of mass
// All children are stored as pointers to the nodes in the tree vector.

enum node_type {
	EMPTY,
	LEAF,
	INTERNAL
};

template<typename T, usize D>
union node_data {
	struct { // Leaf node
		T mass; // Mass
		vector<T, D> com; // Center of mass
	};
	struct { // Internal node
		T tmass; // Total mass
		vector<T, D> wcom; // Weighted center of mass (tmass * com)
		usize children[1 << D]; // Children
	};
	// Empty node has no data
};

template<typename T, usize D>
struct node {
	node_type type;
	node_data<T, D> data;
};

// Quadrants for 2D
//    y
//  2 │ 3
// ───┼─── x
//  0 │ 1
// TODO: Test for 3D
template<typename T, usize D>
struct bounds {
	vector<T, D> center;
	T size;

	[[nodiscard]] usize quadrant(const vector<T, D> &pos) const {
		usize result = 0;
		for (usize i = 0; i < D; i++) {
			result |= (pos[i] > center[i]) << i;
		}
		return result;
	}

	[[nodiscard]] bounds<T, D> divide(usize q) const {
		bounds<T, D> result;
		result.size = size / 2;
		for (usize i = 0; i < D; i++) {
			result.center[i] =
				center[i] + (q & (1 << i) ? 1 : -1) * result.size;
		}
		return result;
	}
};

template<typename T, usize D>
class tree {
public:
	explicit tree(usize count, T theta = 0.5, bounds<T, D> bounds = {{}, {}})
		: _bounds(bounds), _theta(theta) {
		_nodes.reserve(count);
		_nodes.push_back({EMPTY, {}}); // Root node
	}

	// Insert a mass at a position
	void insert(T mass, const vector<T, D> &pos) {
		_insert(0, mass, pos, _bounds);
	}

	// Compute the acceleration
	vector<T, D> evaluate(const vector<T, D> &pos) {
		return -6.67430e-11 * _evaluate(0, pos, _bounds, _theta);
	}

	void reset() {
		_nodes.clear();
		_nodes.push_back({EMPTY, {}}); // Root node
	}

	// For debugging, recursively print the tree
	void print(usize index, usize depth = 0) {
		static const usize max_depth = 10;
		static char spacing[2 * max_depth] = {0};

		if (depth >= max_depth)
			return;

		for (usize i = 0; i < depth; i++) {
			spacing[2 * i] = '|';
			spacing[2 * i + 1] = ' ';
		}
		spacing[2 * depth] = '\0';

		if (_nodes[index].type == EMPTY) {
			LOG_DEBUG("{}Empty", spacing);
			return;
		} else if (_nodes[index].type == LEAF) {
			LOG_DEBUG("{}Leaf: mass = {:.2e}, com = ({}, {})",
					  spacing,
					  _nodes[index].data.mass,
					  _nodes[index].data.com[0],
					  _nodes[index].data.com[1]);
		} else {
			LOG_DEBUG("{}Internal: tmass = {:.2e}, wcom = ({}, {})",
					  spacing,
					  _nodes[index].data.tmass,
					  _nodes[index].data.wcom[0],
					  _nodes[index].data.wcom[1]);
			for (usize i = 0; i < (1 << D); i++)
				print(_nodes[index].data.children[i], depth + 1);
		}
	}

	void set_bounds(bounds<T, D> bounds) {
		_bounds = bounds;
	}

private:
	void _insert(usize index,
				 T mass,
				 const vector<T, D> &pos,
				 bounds<T, D> bnd) {
		if (_nodes[index].type == EMPTY) {
			_nodes[index].type = LEAF;
			_nodes[index].data.mass = mass;
			_nodes[index].data.com = pos;
		} else if (_nodes[index].type == LEAF) {
			T mass1 = _nodes[index].data.mass;
			vector<T, D> pos1 = _nodes[index].data.com;
			usize q1 = bnd.quadrant(pos1);
			bounds<T, D> bnd1 = bnd.divide(q1);

			T mass2 = mass;
			vector<T, D> pos2 = pos;
			usize q2 = bnd.quadrant(pos2);
			bounds<T, D> bnd2 = bnd.divide(q2);

			// Convert leaf node to internal node
			_nodes[index].type = INTERNAL;
			_nodes[index].data.tmass = mass1 + mass2;
			_nodes[index].data.wcom = pos1 * mass1 + pos2 * mass2;

			// Allocate children
			for (usize i = 0; i < (1 << D); i++) {
				usize child = _nodes.size();
				_nodes.push_back({EMPTY, {}});
				_nodes[index].data.children[i] = child;
			}

			// Insert old leaf node then new leaf node
			_insert(_nodes[index].data.children[q1], mass1, pos1, bnd1);
			_insert(_nodes[index].data.children[q2], mass2, pos2, bnd2);
		} else if (_nodes[index].type == INTERNAL) {
			usize q0 = bnd.quadrant(pos);
			bounds<T, D> bnd0 = bnd.divide(q0);

			// Add the weight of the new mass to the weighted center of mass
			_nodes[index].data.wcom += pos * mass;
			_nodes[index].data.tmass += mass;

			_insert(_nodes[index].data.children[q0], mass, pos, bnd0);
		}
	}

	vector<T, D> _evaluate(usize index,
						   const vector<T, D> &pos,
						   bounds<T, D> bnd,
						   T theta) {
		if (_nodes[index].type == EMPTY) {
			return vector<T, D>();
		} else if (_nodes[index].type == LEAF) {
			// Check if the leaf node is the same as the position
			// TODO: Use epsilon
			if ((_nodes[index].data.com - pos).length_squared() < 1e-3) {
				return vector<T, D>();
			} else {
				vector<T, D> r = pos - _nodes[index].data.com;
				T d2 = r.length_squared();
				T d = sqrt(d2);
				return r * _nodes[index].data.mass / (d2 * d);
			}
		} else if (_nodes[index].type == INTERNAL) {
			vector<T, D>
				r = pos - _nodes[index].data.wcom / _nodes[index].data.tmass;
			T d2 = r.length_squared();
			T d = sqrt(d2);
			if (bnd.size / d < theta) {
				// Far enough, use center of mass
				return r * _nodes[index].data.tmass / (d2 * d);
			} else {
				// Recursively evaluate children
				vector<T, D> result;
				for (usize i = 0; i < (1 << D); i++) {
					bounds<T, D> bnd0 = bnd.divide(i);
					result += _evaluate(_nodes[index].data.children[i],
										pos,
										bnd0,
										theta);
				}
				return result;
			}
		}
	}

private:
	T _theta;
	bounds<T, D> _bounds;
	std::vector<node<T, D>> _nodes;
};

template<typename T, usize D>
class engine_barneshut : public engine_base<T, 2, D> {
public:
	engine_barneshut(usize count, std::vector<T> mass)
		: engine_base<T, 2, D>(count), _mass(count), _tree(count * 5) {
		std::copy(mass.begin(), mass.end(), _mass.begin());
	}

	buffer buffer_create(usize size);
	void buffer_destroy(buffer &buffer);
	void buffer_copy(buffer &dst, buffer &src);
	void buffer_fill(buffer &buffer, vector<T, 2 * D> value);
	void buffer_write(const buffer &buffer,
					  vector<T, 2 * D> *data,
					  usize count);
	void buffer_read(vector<T, 2 * D> *data,
					 const buffer &buffer,
					 usize count);

	void compute_f(T time, buffer &y, buffer &dy);
	void compute_mul_add(buffer &x, buffer &y, buffer &z, T alpha);
	void compute_mul_add_inplace(buffer &x, buffer &y, T alpha);
	void compute_mul(buffer &x, buffer &y, T alpha);
	void compute_mul_inplace(buffer &x, T alpha);

protected:
	tree<T, D> _tree;
	std::vector<T> _mass;
};

template<typename T, usize D>
buffer engine_barneshut<T, D>::buffer_create(usize size) {
	buffer b{malloc(size), size};
	return b;
}

template<typename T, usize D>
void engine_barneshut<T, D>::buffer_destroy(buffer &buffer) {
	free(buffer.data);
}

template<typename T, usize D>
void engine_barneshut<T, D>::buffer_copy(buffer &dst, buffer &src) {
	memcpy(dst.data, src.data, src.size);
}

template<typename T, usize D>
void engine_barneshut<T, D>::buffer_fill(buffer &buffer,
										 vector<T, 2 * D> value) {
	auto *data = static_cast<vector<T, 2 * D> *>(buffer.data);
	usize count = buffer.size / sizeof(*data);
	std::fill(data, data + count, value);
}

template<typename T, usize D>
void engine_barneshut<T, D>::buffer_write(const buffer &buffer,
										  vector<T, 2 * D> *data,
										  usize count) {
	auto *dst = static_cast<vector<T, 2 * D> *>(buffer.data);
	std::copy(data, data + count, dst);
}

template<typename T, usize D>
void engine_barneshut<T, D>::buffer_read(vector<T, 2 * D> *data,
										 const buffer &buffer,
										 usize count) {
	auto *src = static_cast<vector<T, 2 * D> *>(buffer.data);
	std::copy(src, src + count, data);
}

template<typename T, usize D>
void engine_barneshut<T, D>::compute_f(T time, buffer &y, buffer &dy) {
	auto *data_y = static_cast<vector<T, D> *>(y.data);
	auto *data_dy = static_cast<vector<T, D> *>(dy.data);
	usize n = y.size / sizeof(*data_y);

	// Compute bounds
	vector<f64, 2> pmin{1.0 / 0.0, 1.0 / 0.0};
	vector<f64, 2> pmax{-1.0 / 0.0, -1.0 / 0.0};
	for (usize i = 0; i < n; i += 2) {
		pmin = pmin.min(data_y[i]);
		pmax = pmax.max(data_y[i]);
	}

	vector<f64, 2> center = (pmin + pmax) / 2;
	f64 size = (pmax - pmin).max_element();
	bounds<f64, 2> bounds{center, size};

	_tree.set_bounds(bounds);

	// Build the tree
	_tree.reset();
	for (usize i = 0; i < n; i += 2) {
		_tree.insert(_mass[i / 2], data_y[i]);
	}

	// Compute the acceleration
#pragma omp parallel for
	for (usize i = 0; i < n; i += 2) {
		data_dy[i] = data_y[i + 1];
		data_dy[i + 1] = _tree.evaluate(data_y[i]);
	}
}

template<typename T, usize D>
void engine_barneshut<T, D>::compute_mul_add(buffer &x,
											 buffer &y,
											 buffer &z,
											 T alpha) {
	auto *data_x = static_cast<vector<T, 2 * D> *>(x.data);
	auto *data_y = static_cast<vector<T, 2 * D> *>(y.data);
	auto *data_z = static_cast<vector<T, 2 * D> *>(z.data);
	usize count = x.size / sizeof(*data_x);
#pragma omp parallel for
	for (usize i = 0; i < count; i++)
		data_x[i] = data_y[i] + alpha * data_z[i];
}

template<typename T, usize D>
void engine_barneshut<T, D>::compute_mul_add_inplace(buffer &x,
													 buffer &y,
													 T alpha) {
	auto *data_x = static_cast<vector<T, 2 * D> *>(x.data);
	auto *data_y = static_cast<vector<T, 2 * D> *>(y.data);
	usize count = x.size / sizeof(*data_x);
#pragma omp parallel for
	for (usize i = 0; i < count; i++)
		data_x[i] += alpha * data_y[i];
}

template<typename T, usize D>
void engine_barneshut<T, D>::compute_mul(buffer &x, buffer &y, T alpha) {
	auto *data_x = static_cast<vector<T, 2 * D> *>(x.data);
	auto *data_y = static_cast<vector<T, 2 * D> *>(y.data);
	usize count = x.size / sizeof(*data_x);
#pragma omp parallel for
	for (usize i = 0; i < count; i++)
		data_x[i] = alpha * data_y[i];
}

template<typename T, usize D>
void engine_barneshut<T, D>::compute_mul_inplace(buffer &x, T alpha) {
	auto *data_x = static_cast<vector<T, 2 * D> *>(x.data);
	usize count = x.size / sizeof(*data_x);
#pragma omp parallel for
	for (usize i = 0; i < count; i++)
		data_x[i] *= alpha;
}

#endif // ENGINE_BARNESHUT_HPP
