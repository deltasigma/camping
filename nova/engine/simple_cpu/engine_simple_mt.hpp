#ifndef ENGINE_SIMPLE_MT_HPP
# define ENGINE_SIMPLE_MT_HPP

# include <cstring>
# include <omp.h>
# include "types.hpp"
# include "math/vec.hpp"
# include "engine/engine.hpp"
# include "util/log.hpp"
# include "engine_simple.hpp"

template<typename T, usize D>
class engine_simple_mt : public engine_simple<T, D> {
public:
	using engine_simple<T, D>::engine_simple; // Inherit constructors

	void compute_f(T time, buffer &y, buffer &dy);
	void compute_mul_add(buffer &x, buffer &y, buffer &z, T alpha);
	void compute_mul_add_inplace(buffer &x, buffer &y, T alpha);
	void compute_mul(buffer &x, buffer &y, T alpha);
	void compute_mul_inplace(buffer &x, T alpha);
};

template<typename T, usize D>
void engine_simple_mt<T, D>::compute_f(T time, buffer &y, buffer &dy) {
	auto *data_y = static_cast<vector<T, D> *>(y.data);
	auto *data_dy = static_cast<vector<T, D> *>(dy.data);
	usize n = y.size / sizeof(*data_y);

#pragma omp parallel
#pragma omp for schedule(static)
	for (usize i = 0; i < n; i += 2) {
		// r' = v
		data_dy[i] = data_y[i + 1];

		// v' = F(r) / m
		data_dy[i + 1] = vector<T, D>();
		for (usize j = 0; j < n; j += 2) {
			if (i != j) {
				vector<T, D> r = data_y[i] - data_y[j];
				T d2 = r.length_squared();
				T d = sqrt(d2);

				auto mj = ((T *)this->m.data)[j / 2];
				data_dy[i + 1] += mj * r / (d2 * d);
			}
		}
		data_dy[i + 1] *= -6.67430e-11; // G
	}
}

template<typename T, usize D>
void engine_simple_mt<T, D>::compute_mul_add(buffer &x,
											 buffer &y,
											 buffer &z,
											 T alpha) {
	auto *data_x = static_cast<vector<T, 2 * D> *>(x.data);
	auto *data_y = static_cast<vector<T, 2 * D> *>(y.data);
	auto *data_z = static_cast<vector<T, 2 * D> *>(z.data);
	usize count = x.size / sizeof(*data_x);

#pragma omp parallel
#pragma omp for schedule(static)
	for (usize i = 0; i < count; i++)
		data_x[i] = data_y[i] + alpha * data_z[i];
}

template<typename T, usize D>
void engine_simple_mt<T, D>::compute_mul_add_inplace(buffer &x,
													 buffer &y,
													 T alpha) {
	auto *data_x = static_cast<vector<T, 2 * D> *>(x.data);
	auto *data_y = static_cast<vector<T, 2 * D> *>(y.data);
	usize count = x.size / sizeof(*data_x);

#pragma omp parallel
#pragma omp for schedule(static)
	for (usize i = 0; i < count; i++)
		data_x[i] += alpha * data_y[i];
}

template<typename T, usize D>
void engine_simple_mt<T, D>::compute_mul(buffer &x, buffer &y, T alpha) {
	auto *data_x = static_cast<vector<T, 2 * D> *>(x.data);
	auto *data_y = static_cast<vector<T, 2 * D> *>(y.data);
	usize count = x.size / sizeof(*data_x);

#pragma omp parallel
#pragma omp for schedule(static)
	for (usize i = 0; i < count; i++)
		data_x[i] = alpha * data_y[i];
}

template<typename T, usize D>
void engine_simple_mt<T, D>::compute_mul_inplace(buffer &x, T alpha) {
	auto *data_x = static_cast<vector<T, 2 * D> *>(x.data);
	usize count = x.size / sizeof(*data_x);

#pragma omp parallel
#pragma omp for schedule(static)
	for (usize i = 0; i < count; i++)
		data_x[i] *= alpha;
}

#endif // ENGINE_SIMPLE_MT_HPP
