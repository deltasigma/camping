#ifndef ENGINE_SIMPLE_HPP
# define ENGINE_SIMPLE_HPP

# include <cstring>
# include "types.hpp"
# include "math/vec.hpp"
# include "engine/engine.hpp"
# include "util/log.hpp"

// TODO: Use std::vector instead of raw pointers
template<typename T, usize D>
class engine_simple : public engine_base<T, 2, D> {
public:
	engine_simple(usize count, const T mass[])
		: engine_base<T, 2, D>(count) {
		m = buffer_create(count * sizeof(T));
		T *data = static_cast<T *>(m.data);
		for (usize i = 0; i < count; i++)
			data[i] = mass[i];
	}

	~engine_simple() {
		buffer_destroy(m);
	}

	buffer buffer_create(usize size);
	void buffer_destroy(buffer &buffer);
	void buffer_copy(buffer &dst, buffer &src);
	void buffer_fill(buffer &buffer, vector<T, 2 * D> value);
	void buffer_write(const buffer &buffer,
					  vector<T, 2 * D> *data,
					  usize count);
	void buffer_read(vector<T, 2 * D> *data,
					 const buffer &buffer,
					 usize count);

protected:
	buffer m;
};

template<typename T, usize D>
buffer engine_simple<T, D>::buffer_create(usize size) {
	buffer buffer = {nullptr, 0};
	buffer.size = size;
	buffer.data = malloc(size); // TODO: Handle allocation failure
	return buffer;
}

template<typename T, usize D>
void engine_simple<T, D>::buffer_destroy(buffer &buffer) {
	free(buffer.data);
	buffer.data = nullptr;
	buffer.size = 0;
}

template<typename T, usize D>
void engine_simple<T, D>::buffer_copy(buffer &dst, buffer &src) {
	memcpy(dst.data, src.data, src.size);
}

template<typename T, usize D>
void engine_simple<T, D>::buffer_fill(buffer &buffer,
									  vector<T, 2 * D> value) {
	auto *data = static_cast<vector<T, 2 * D> *>(buffer.data);
	usize count = buffer.size / sizeof(*data);
	for (usize i = 0; i < count; i++)
		data[i] = value;
}

template<typename T, usize D>
void engine_simple<T, D>::buffer_write(const buffer &buffer,
									   vector<T, 2 * D> *data,
									   usize count) {
	auto *data_buffer = static_cast<vector<T, 2 * D> *>(buffer.data);
	for (usize i = 0; i < count; i++)
		data_buffer[i] = data[i];
}

template<typename T, usize D>
void engine_simple<T, D>::buffer_read(vector<T, 2 * D> *data,
									  const buffer &buffer,
									  usize count) {
	auto *data_buffer = static_cast<vector<T, 2 * D> *>(buffer.data);
	for (usize i = 0; i < count; i++)
		data[i] = data_buffer[i];
}

#endif // ENGINE_SIMPLE_HPP
