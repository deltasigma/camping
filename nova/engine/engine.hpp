#ifndef ENGINE_HPP
# define ENGINE_HPP

# include "types.hpp"
# include "math/vec.hpp"

// This class implements a generic engine that can be used to solve ordinary
// differential equations. It is designed specifically for the purpose of
// solving the equations of motion for n-body simulations (but it may be
// expanded to support hydrodynamical simulations in the future for cosmology
// simulations).

// The engine is designed to be as generic as possible, so the api it exposes
// can be run on multiple platforms (e.g. CPU, MPI, CUDA, OpenCL, etc.).

// It can be used to solve equations in the form of:
//   dy/dt = f(t, y)
// where y is the state vector and f is the function that defines the
// differential equations.

// Note: The engine api is not intended to be used directly by the user, but
// rather called by the solver implementation.

// Memory:
// In the case of a first order differential equation, the state vector y is
// composed of a single vector of dimension n. For a second order differential
// equation, the state vector y is composed of two vectors of dimension n. In
// general, the state vector y is composed of m vectors of dimension n.
// The engine runs an array of state vectors y to solve in parallel the
// equations of motion for k bodies.
// Memory mapping of the state vector y is as follows:
//   y = [(y0_0, y0_1, ..., y0_n),
//        (y1_0, y1_1, ..., y1_n),
//        ...,
//        (ym_0, ym_1, ..., ym_n)] * k

// Let's take an example of a 2-body problem with 2 dimensions (x, y).
// This problem is a second order differential equation, so the state vector y
// is composed of two vectors of dimension 2. The state vector y is composed of
// the position r and velocity v vectors of the two bodies.
// The memory mapping of the state vector y is as follows:
//   y = [(r0_x, r0_y), (v0_x, v0_y)] [r1_x, r1_y), (v1_x, v1_y)]

enum class dimension {
	ONE_DIMENSIONAL = 1,
	TWO_DIMENSIONAL = 2,
	THREE_DIMENSIONAL = 3
};

enum class order {
	FIRST_ORDER = 1,
	SECOND_ORDER = 2
};

struct buffer {
	void *data;
	usize size;
};

template<typename T, usize O, usize D>
class engine_base {
public:
	usize count; // depth of the problem

	explicit engine_base(usize count);
	virtual ~engine_base() = default;

	// Memory management
	virtual buffer buffer_create(usize size) = 0;
	virtual void buffer_destroy(buffer &buffer) = 0;
	virtual void buffer_copy(buffer &dst, buffer &src) = 0;
	virtual void buffer_fill(buffer &buffer, vector<T, D * O> value) = 0;
	virtual void buffer_write(const buffer &buffer,
							  vector<T, D * O> *data,
							  usize count) = 0;
	virtual void buffer_read(vector<T, D * O> *data,
							 const buffer &buffer,
							 usize count) = 0;

	// Solver
	virtual void compute_f(T time, buffer &y, buffer &dy) = 0;
	virtual void compute_mul_add(buffer &x, buffer &y, buffer &z, T alpha) = 0;
	virtual void compute_mul_add_inplace(buffer &x, buffer &y, T alpha) = 0;
	virtual void compute_mul(buffer &x, buffer &y, T alpha) = 0;
	virtual void compute_mul_inplace(buffer &x, T alpha) = 0;

	// Accessors
	[[nodiscard]] usize buffer_size() const;
};

template<typename T, usize O, usize D>
engine_base<T, O, D>::engine_base(usize count) : count(count) {}

template<typename T, usize O, usize D>
usize engine_base<T, O, D>::buffer_size() const {
	return count * D * O * sizeof(T);
}

#endif // ENGINE_HPP
