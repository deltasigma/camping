# 🏕 Camping

**C**osmic **A**nalysis **M**assively **P**arallel **I**ntegrator for **N**-body **G**ravitation

Welcome to **Camping**, a state-of-the-art parallel N-body integrator designed to simulate the gravitational interactions of a vast number of particles across cosmic scales. Whether you're delving into the mysteries of the universe, exploring alternate theories of gravity, or engaging in any other N-body simulations, Camping offers the tools and computational power you need to achieve your goals.

| ![Portrait of Jean-Pierre Petit](https://upload.wikimedia.org/wikipedia/commons/7/73/JPPetit.jpg) | Originally developed to explore and validate alternative gravitational theories, such as the fascinating Janus bi-metric theory by [Jean-Pierre Petit](https://en.wikipedia.org/wiki/Jean-Pierre_Petit_(physicist)), Camping now spearheads advancements in computational astronomy and astrophysics. |
|-|-|

**Camping** is implemented in efficient, plain C and supports a variety of computing backends and integrators to accommodate different research needs and hardware environments.

## 🌌 Features
- **Massive Parallelism**: Harness the power of modern computing clusters to conduct large-scale simulations.
- **Multiple Backends**: Opt for one of several computing backends, including MPI & HIP, to achieve optimal performance on various hardware setups.
- **Flexible Integrators**: Choose from an array of integrators tailored to your simulation requirements.

## 🚀 Getting Started

### Prerequisites
Before you embark on using Camping, ensure you have the following installed:
- A C compiler (GCC recommended)
- MPI library for parallel computing
- HIP framework (for AMD GPUs) or other applicable backends for your hardware
- CMake for building the software

### 🛠 Installation
Clone the repository and compile the project using CMake:
```bash
git clone https://github.com/deltasigma/camping.git
cd camping
mkdir build && cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
cmake --build .
```

### 🏃 Running Simulations
TODO: Add instructions on how to run simulations

## 📖 Documentation
For more detailed documentation, including full installation guides, backend configurations, and examples of typical simulations, please refer to the `docs/` directory.

## 🤝 Contributing
We welcome contributions from the community. If you're looking to contribute, please read the `CONTRIBUTING.md` file for more information on how to get started.

## 🔍 Feature Matrix
Below is a feature matrix that provides a snapshot of the capabilities and configurations supported by Camping:

Legend:
 - ❌: Not yet supported but planned
 - ⚠️: Implementation in progress
 - ✅: Fully supported

### Integrator Solvers

| Method | Supported | Order | Adaptive | Description |
|--------|-----------|-------|----------|-------------|
| Adams-Bashforth | ❌ | up to 5 | ➖ | |
| Bulirsch-Stoer | ❌ | 2 x max level | ⭐ | |
| Euler | ⚠️ | 1 | ➖ | |
| Runge-Kutta 4th order | ❌ | 4 | ➖ | |
| Runge-Kutta-Cash-Karp 5th order | ❌ | 5 | ⭐ | |
| Runge-Kutta-Dormand-Prince 5th order | ❌ | 5 | ⭐ | |
| Runge-Kutta-Fehlberg 7th order | ❌ | 7 | ⭐ | |
| Runge-Kutta-Feagin 10th order | ❌ | 10 | ⭐ | |
| Runge-Kutta-Feagin 12th order | ❌ | 12 | ⭐ | |
| Runge-Kutta-Feagin 14th order | ❌ | 14 | ⭐ | |
| Verlet | ❌ | 2 | ➖ | |

### Gravitational Models

| Model | Supported | Description |
|-------|-----------|-------------|
| Newtonian | ⚠️ | Standard Newtonian gravity |
| Janus | ❌ | Janus bi-metric theory by Jean-Pierre Petit |

### Backend Engines
Backend engines are crucial for parallelizing the simulation across different hardware. Each combination includes a backend and an acceleration algorithm.

| Backend | Acceleration | Supported | Description |
|---------|--------------|-----------|-------------|
| Single-threaded | Naive | ⚠️ | Single-threaded simulation |
| Multi-threaded OpenMP | Naive | ❌ | Multi-threaded simulation using OpenMP |
| Multi-threaded MPI | Naive | ❌ | Multi-threaded simulation using MPI |
| Single-threaded | Barnes-Hut | ❌ | Single-threaded simulation with Barnes-Hut algorithm |
| Multi-threaded OpenMP | Barnes-Hut | ❌ | Multi-threaded simulation with Barnes-Hut algorithm using OpenMP |
| Multi-threaded MPI | Barnes-Hut | ❌ | Multi-threaded simulation with Barnes-Hut algorithm using MPI |
| Single-threaded | Fast Multipole Method | ❌ | Single-threaded simulation with Fast Multipole Method |
| Multi-threaded OpenMP | Fast Multipole Method | ❌ | Multi-threaded simulation with Fast Multipole Method using OpenMP |
| Multi-threaded MPI | Fast Multipole Method | ❌ | Multi-threaded simulation with Fast Multipole Method using MPI |

## 📜 License
Camping is open-sourced under the GNU General Public License v3 (GPLv3). See the `LICENSE` file for more details.

We hope you find Camping invaluable for your research and simulation endeavors. Happy simulating!
